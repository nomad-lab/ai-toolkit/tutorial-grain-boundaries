import json
from setuptools import setup, find_packages

# https://docs.python.org/3.7/distutils/setupscript.html
# https://docs.python.org/3.7/distutils/apiref.html#distutils.core.setup

with open('metainfo.json') as file:
    metainfo = json.load(file)

setup(
    name='grain_boundaries',
    version='0.9',
    author=', '.join(metainfo['authors']),
    author_email=metainfo['email'],
    url=metainfo['url'],
    description=metainfo['title'],
    long_description=metainfo['description'],
    packages=find_packages(),
    install_requires=['ase', 'asap3', 'numpy', 'scipy', 'scikit-learn', 'matplotlib', 'nglview', 'ipywidgets'],
)

