import json
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt

from ase.io import read


class Calculation(object):
    def __init__(self, *args, **kwargs):
        self.filepath = kwargs.pop('filepath', None)
        self.parameters = kwargs

    def get_data(self, index=-1):
        return read(str(self.filepath), index=index)

    @classmethod
    def from_path(cls, path: Path, index=-1):
        with (path / 'gb.json').open() as data_file:
            gb_data = json.load(data_file)

        with (path / 'subgb.json').open() as data_file:
            subgb_data = json.load(data_file)

        filename = subgb_data['name'] + "_traj.xyz"
        filepath = (path / filename).resolve()

        return cls(**{**gb_data, **subgb_data}, filepath=filepath)


if __name__ == '__main__':

    # Read grain boundary database
    dirpath = Path('../GB_alphaFe_001')

    calculations = {
        'tilt': [Calculation.from_path(calc_dir) for calc_dir in (dirpath / 'tilt').iterdir() if calc_dir.is_dir()],
        'twist': [Calculation.from_path(calc_dir) for calc_dir in (dirpath / 'twist').iterdir() if calc_dir.is_dir()]
    }

    # potential energy of the perfect crystal according to a specific potential
    potential_energy_per_atom = -4.01298214176  # alpha-Fe PotBH
    eV = 1.6021766208e-19
    Angstrom = 1.e-10

    angles, energies = [], []
    for calc in sorted(calculations['tilt'], key=lambda item: item.parameters['angle']):
        angles.append(calc.parameters['angle'] * 180.0 / np.pi)

        energy = 16.02 / (2 * calc.parameters['A']) * \
                 (calc.parameters['E_gb'] - potential_energy_per_atom * calc.parameters['n_at'])

        atoms = calc.get_data()
        cell = atoms.get_cell()
        A = cell[0, 0] * cell[1, 1]

        energy = (
                eV / Angstrom ** 2 /
                (2 * A) *
                (atoms.get_total_energy() - potential_energy_per_atom * len(atoms))
        )

        print(energy)
        energies.append(energy)

    plt.bar(angles, energies)
    plt.show()
